package viajenda.dominio;

public enum TipoEvento{CULTURAL, DEPORTIVO, COMERCIAL, FAMILIAR, LABORAL, OTRO};